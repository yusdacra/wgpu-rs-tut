use crate::shader;
use crate::texture;
use crate::types::*;
use winit::event::WindowEvent;
use winit::window::Window;

pub struct State {
    surface: wgpu::Surface,
    adapter: wgpu::Adapter,
    device: wgpu::Device,
    queue: wgpu::Queue,
    sc_desc: wgpu::SwapChainDescriptor,
    swap_chain: wgpu::SwapChain,

    render_pipeline: wgpu::RenderPipeline,

    vertex_buffer: wgpu::Buffer,
    index_buffer: wgpu::Buffer,
    instance_buffer: wgpu::Buffer,
    uniform_buffer: wgpu::Buffer,
    uniform_bind_group: wgpu::BindGroup,

    depth_texture: texture::Texture,
    diffuse_texture: texture::Texture,
    diffuse_bind_group: wgpu::BindGroup,

    instances: Vec<Instance>,
    uniforms: Uniforms,
    camera: Camera,
    camera_controller: CameraController,
    size: winit::dpi::PhysicalSize<u32>,
    clear_color: wgpu::Color,
    num_indices: u32,
    start_time: std::time::Instant,
}

impl State {
    pub async fn new(window: &Window) -> Result<Self, failure::Error> {
        let size = window.inner_size();
        let surface = wgpu::Surface::create(window);
        let adapter = wgpu::Adapter::request(
            &wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::Default,
                compatible_surface: Some(&surface),
            },
            wgpu::BackendBit::PRIMARY,
        )
        .await
        .ok_or(Error::AdapterRequestFailed)?;

        let (device, queue) = adapter
            .request_device(&wgpu::DeviceDescriptor {
                extensions: wgpu::Extensions {
                    anisotropic_filtering: false,
                },
                limits: Default::default(),
            })
            .await;

        let sc_desc = wgpu::SwapChainDescriptor {
            usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT,
            format: wgpu::TextureFormat::Bgra8UnormSrgb,
            width: size.width,
            height: size.height,
            present_mode: wgpu::PresentMode::Mailbox,
        };
        let swap_chain = device.create_swap_chain(&surface, &sc_desc);

        // TEXTURE
        let (diffuse_texture, cmd_buffer) =
            texture::Texture::from_file(&device, "src/happy-tree.png", Some("happy-tree"))?;
        let depth_texture =
            texture::Texture::create_depth_texture(&device, &sc_desc, "depth_texture");
        queue.submit(&[cmd_buffer]);

        let texture_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                bindings: &[
                    wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStage::FRAGMENT,
                        ty: wgpu::BindingType::SampledTexture {
                            multisampled: false,
                            dimension: wgpu::TextureViewDimension::D2,
                            component_type: wgpu::TextureComponentType::Uint,
                        },
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 1,
                        visibility: wgpu::ShaderStage::FRAGMENT,
                        ty: wgpu::BindingType::Sampler { comparison: false },
                    },
                ],
                label: Some("texture_bind_group_layout"),
            });

        let diffuse_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &texture_bind_group_layout,
            bindings: &[
                wgpu::Binding {
                    binding: 0,
                    resource: wgpu::BindingResource::TextureView(&diffuse_texture.view()),
                },
                wgpu::Binding {
                    binding: 1,
                    resource: wgpu::BindingResource::Sampler(&diffuse_texture.sampler()),
                },
            ],
            label: Some("diffuse_bind_group"),
        });
        // TEXTURE

        // SHADER
        let mut compiler = shaderc::Compiler::new().expect("Failed to create shaderc compiler");
        let mut vs =
            shader::Shader::from_file(shader::Kind::Vertex, "src/shader.vert", "main").unwrap();
        vs.cook(&mut compiler, &device)?;
        let mut fs =
            shader::Shader::from_file(shader::Kind::Fragment, "src/shader.frag", "main").unwrap();
        fs.cook(&mut compiler, &device)?;
        // SHADER

        let vertex_buffer = device
            .create_buffer_with_data(bytemuck::cast_slice(VERTICES), wgpu::BufferUsage::VERTEX);

        let index_buffer =
            device.create_buffer_with_data(bytemuck::cast_slice(INDICES), wgpu::BufferUsage::INDEX);

        let camera = Camera {
            eye: (0.0, 1.0, 2.0).into(),
            target: (0.0, 0.0, 0.0).into(),
            up: V3::unit_y(),
            aspect: sc_desc.width as f32 / sc_desc.height as f32,
            fovy: 45.0,
            znear: 0.1,
            zfar: 100.0,
        };

        let instances = (0..NUM_INSTANCES_PER_ROW)
            .flat_map(|z| {
                (0..NUM_INSTANCES_PER_ROW).map(move |x| {
                    use cgmath::num_traits::Zero;
                    use cgmath::InnerSpace;
                    use cgmath::Rotation3;
                    let pos = V3::new(x as f32, 0.0, z as f32) - INSTANCE_DISPLACEMENT;
                    let rot = if pos.is_zero() {
                        Q::from_axis_angle(V3::unit_z(), cgmath::Deg(0.0))
                    } else {
                        Q::from_axis_angle(pos.clone().normalize(), cgmath::Deg(45.0))
                    };

                    Instance::new(pos, rot)
                })
            })
            .collect::<Vec<_>>();

        let instance_data = instances.iter().map(Instance::to_raw).collect::<Vec<_>>();
        let instance_buffer = device.create_buffer_with_data(
            bytemuck::cast_slice(&instance_data),
            wgpu::BufferUsage::VERTEX | wgpu::BufferUsage::COPY_DST,
        );

        let uniforms = Uniforms::new(camera.build_vp_matrix());
        let uniform_buffer = device.create_buffer_with_data(
            bytemuck::cast_slice(&[uniforms]),
            wgpu::BufferUsage::UNIFORM | wgpu::BufferUsage::COPY_DST,
        );

        let uniform_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                bindings: &[wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStage::VERTEX,
                    ty: wgpu::BindingType::UniformBuffer { dynamic: false },
                }],
                label: Some("uniform_bind_group_layout"),
            });

        let uniform_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &uniform_bind_group_layout,
            bindings: &[wgpu::Binding {
                binding: 0,
                resource: wgpu::BindingResource::Buffer {
                    buffer: &uniform_buffer,
                    range: 0..std::mem::size_of_val(&uniforms) as wgpu::BufferAddress,
                },
            }],
            label: Some("uniform_bind_group"),
        });

        let render_pipeline_layout =
            device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                bind_group_layouts: &[&texture_bind_group_layout, &uniform_bind_group_layout],
            });

        let render_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            layout: &render_pipeline_layout,
            vertex_stage: wgpu::ProgrammableStageDescriptor {
                module: vs.module().ok_or(shader::Error::ModuleNotFound)?,
                entry_point: "main",
            },
            fragment_stage: Some(wgpu::ProgrammableStageDescriptor {
                module: fs.module().ok_or(shader::Error::ModuleNotFound)?,
                entry_point: "main",
            }),
            rasterization_state: Some(wgpu::RasterizationStateDescriptor {
                front_face: wgpu::FrontFace::Ccw,
                cull_mode: wgpu::CullMode::Back,
                depth_bias: 0,
                depth_bias_slope_scale: 0.0,
                depth_bias_clamp: 0.0,
            }),
            color_states: &[wgpu::ColorStateDescriptor {
                format: sc_desc.format,
                color_blend: wgpu::BlendDescriptor::REPLACE,
                alpha_blend: wgpu::BlendDescriptor::REPLACE,
                write_mask: wgpu::ColorWrite::ALL,
            }],
            primitive_topology: wgpu::PrimitiveTopology::TriangleList,
            depth_stencil_state: Some(wgpu::DepthStencilStateDescriptor {
                format: texture::DEPTH_FORMAT,
                depth_write_enabled: true,
                depth_compare: wgpu::CompareFunction::Less,
                stencil_front: wgpu::StencilStateFaceDescriptor::IGNORE,
                stencil_back: wgpu::StencilStateFaceDescriptor::IGNORE,
                stencil_read_mask: 0,
                stencil_write_mask: 0,
            }),
            vertex_state: wgpu::VertexStateDescriptor {
                index_format: wgpu::IndexFormat::Uint16,
                vertex_buffers: &[Vertex::desc(), InstanceRaw::desc()],
            },
            sample_count: 1,
            sample_mask: !0,
            alpha_to_coverage_enabled: false,
        });

        Ok(Self {
            surface,
            adapter,
            device,
            queue,
            sc_desc,
            swap_chain,
            render_pipeline,

            vertex_buffer,
            index_buffer,
            instance_buffer,
            uniform_buffer,
            uniform_bind_group,

            depth_texture,
            diffuse_texture,
            diffuse_bind_group,

            instances,
            uniforms,
            camera,
            camera_controller: CameraController::new(0.2),
            size,
            clear_color: wgpu::Color::BLACK,
            num_indices: INDICES.len() as u32,
            start_time: std::time::Instant::now(),
        })
    }

    pub fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
        self.size = new_size;
        self.sc_desc.width = self.size.width;
        self.sc_desc.height = self.size.height;
        self.swap_chain = self.device.create_swap_chain(&self.surface, &self.sc_desc);
        self.depth_texture =
            texture::Texture::create_depth_texture(&self.device, &self.sc_desc, "depth_texture");
    }

    // input() won't deal with GPU code, so it can be synchronous
    pub fn input(&mut self, event: &WindowEvent) -> bool {
        match event {
            WindowEvent::CursorMoved { position, .. } => {
                self.clear_color = wgpu::Color {
                    r: position.x / self.size.width as f64 * 0.5_f64,
                    g: position.y / self.size.height as f64 * 0.5_f64,
                    b: 0.0,
                    a: 1.0,
                };
                true
            }
            _ => self.camera_controller.process_events(event),
        }
    }

    pub fn update(&mut self) {
        self.camera_controller.update_camera(&mut self.camera);
        self.uniforms.update_vp(self.camera.build_vp_matrix());
        let new_rot_deg = cgmath::Deg(((self.start_time.elapsed().as_secs() % 36) * 10) as f32);
        for i in &mut self.instances {
            use cgmath::num_traits::Zero;
            use cgmath::InnerSpace;
            use cgmath::Rotation3;
            i.set_rot(if i.pos().is_zero() {
                Q::from_axis_angle(V3::unit_z(), new_rot_deg)
            } else {
                Q::from_axis_angle(i.pos().clone().normalize(), new_rot_deg)
            });
        }

        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("update_encoder"),
            });

        encoder.copy_buffer_to_buffer(
            &self.st_buf(&[self.uniforms]),
            0,
            &self.uniform_buffer,
            0,
            std::mem::size_of::<Uniforms>() as wgpu::BufferAddress,
        );

        let instance_data = self
            .instances
            .iter()
            .map(Instance::to_raw)
            .collect::<Vec<_>>();

        encoder.copy_buffer_to_buffer(
            &self.st_buf(&instance_data),
            0,
            &self.instance_buffer,
            0,
            (std::mem::size_of::<InstanceRaw>() * instance_data.len() as usize)
                as wgpu::BufferAddress,
        );
        self.queue.submit(&[encoder.finish()]);
    }

    pub fn render(&mut self) {
        let frame = self
            .swap_chain
            .get_next_texture()
            .expect("Failed to get texture; might be a timeout");

        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Render Encoder"),
            });

        let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                attachment: &frame.view,
                resolve_target: None,
                load_op: wgpu::LoadOp::Clear,
                store_op: wgpu::StoreOp::Store,
                clear_color: self.clear_color,
            }],
            depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachmentDescriptor {
                attachment: &self.depth_texture.view(),
                depth_load_op: wgpu::LoadOp::Clear,
                depth_store_op: wgpu::StoreOp::Store,
                clear_depth: 1.0,
                stencil_load_op: wgpu::LoadOp::Clear,
                stencil_store_op: wgpu::StoreOp::Store,
                clear_stencil: 0,
            }),
        });
        render_pass.set_pipeline(&self.render_pipeline);
        render_pass.set_bind_group(0, &self.diffuse_bind_group, &[]);
        render_pass.set_bind_group(1, &self.uniform_bind_group, &[]);
        render_pass.set_vertex_buffer(0, &self.vertex_buffer, 0, 0);
        render_pass.set_vertex_buffer(1, &self.instance_buffer, 0, 0);
        render_pass.set_index_buffer(&self.index_buffer, 0, 0);
        render_pass.draw_indexed(0..self.num_indices, 0, 0..NUM_INSTANCES);

        drop(render_pass);

        self.queue.submit(&[encoder.finish()]);
    }

    fn st_buf<T: bytemuck::Pod>(&self, data: &[T]) -> wgpu::Buffer {
        self.device
            .create_buffer_with_data(bytemuck::cast_slice(data), wgpu::BufferUsage::COPY_SRC)
    }
}

#[derive(Debug)]
enum Error {
    AdapterRequestFailed,
}
impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}
impl failure::Fail for Error {}

const VERTICES: &[Vertex] = &[
    Vertex {
        pos: [-0.086_821, 0.492_403, 0.0],
        tex_coords: [0.413_175, 0.007_596],
    }, // A
    Vertex {
        pos: [-0.495_134, 0.069_586, 0.0],
        tex_coords: [0.004_865, 0.430_413],
    }, // B
    Vertex {
        pos: [0.219_185, -0.449_397, 0.0],
        tex_coords: [0.280_814, 0.949_397],
    }, // C
    Vertex {
        pos: [0.495_134, -0.069_586, 0.0],
        tex_coords: [0.004_865, 0.430_413],
    }, // B
];

const INDICES: &[u16] = &[0, 1, 2, 2, 3, 0];

pub const NUM_INSTANCES_PER_ROW: u32 = 2;
pub const NUM_INSTANCES: u32 = NUM_INSTANCES_PER_ROW * NUM_INSTANCES_PER_ROW;
pub const INSTANCE_DISPLACEMENT: cgmath::Vector3<f32> = cgmath::Vector3::new(
    NUM_INSTANCES_PER_ROW as f32 * 0.5,
    0.0,
    NUM_INSTANCES_PER_ROW as f32 * 0.5,
);
