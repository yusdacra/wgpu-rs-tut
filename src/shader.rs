pub type Compiler = shaderc::Compiler;
pub type Kind = shaderc::ShaderKind;

#[derive(Debug)]
pub enum Error {
    ModuleNotFound,
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}
impl failure::Fail for Error {}
pub struct Shader {
    kind: Kind,
    src: String,
    file_name: String,
    entry_point: String,
    module: Option<wgpu::ShaderModule>,
}

impl Shader {
    pub fn new(kind: Kind, src: String, file_name: &str, entry_point: &str) -> Self {
        Self {
            kind,
            src,
            file_name: file_name.to_string(),
            entry_point: entry_point.to_string(),
            module: None,
        }
    }

    pub fn from_file(kind: Kind, path: &str, entry_point: &str) -> Result<Self, failure::Error> {
        let inner_path = std::path::Path::new(path);
        let src = std::fs::read_to_string(inner_path)?;
        Ok(Self::new(
            kind,
            src,
            inner_path.file_name().unwrap().to_str().unwrap(),
            entry_point,
        ))
    }

    pub fn cook(
        &mut self,
        compiler: &mut Compiler,
        device: &wgpu::Device,
    ) -> Result<(), failure::Error> {
        let spirv = compiler.compile_into_spirv(
            &self.src,
            self.kind,
            &self.file_name,
            &self.entry_point,
            None,
        )?;
        let data = wgpu::read_spirv(std::io::Cursor::new(spirv.as_binary_u8()))?;
        self.module = Some(device.create_shader_module(&data));
        Ok(())
    }

    pub fn module(&self) -> Option<&wgpu::ShaderModule> {
        self.module.as_ref()
    }
}
